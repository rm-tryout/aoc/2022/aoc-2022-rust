use crate::{Grid, get_scenic_score_for_tree};

pub fn process_part_02(input:&str) -> usize {
  let grid = input.parse::<Grid>().unwrap();

  grid.values
  .iter()
  .map(|(c,(_height, _v))|{
    get_scenic_score_for_tree(&grid, c)
  })
  .max()
  .unwrap()
}