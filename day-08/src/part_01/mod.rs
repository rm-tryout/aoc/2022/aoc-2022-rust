use crate::{Grid, Coord};

pub fn process_part_01(input: &str) -> usize{
  let grid = input.parse::<Grid>().unwrap();

  grid.values
  .iter()
  .filter(|(Coord(_x,_y),(_height, visible))|{
    *visible
  })
  .count()
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn part_01_with_test_input(){
    let result = process_part_01(TEST_INPUT);
    assert_eq!(result,21 )
  }
}