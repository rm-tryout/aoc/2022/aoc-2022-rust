use std::{collections::HashMap, ops::{ Add, Mul }, hash::Hash, str::FromStr};

pub mod part_01;
pub mod part_02;

#[derive(PartialEq, Eq, Hash, Debug)]
struct Coord(isize,isize);
impl Coord {
  fn new_usize(a:usize, b:usize) -> Self {
    Self(a as isize, b as isize)
  }
}

impl Add for Coord {
  type Output = Coord;

  fn add(self, rhs: Self) -> Self::Output {
      Self(self.0 + rhs.0, self.1+ rhs.1)
  }
}

impl Mul<isize> for Coord {
  type Output = Coord;
  fn mul(self, rhs: isize) -> Self::Output {
      Self(self.0*rhs, self.1*rhs)
  }
}

struct Grid {
  values: HashMap<Coord, (
    usize,            // height
    bool              // visible
  )>,
  width: usize,
  height: usize
}

struct Direction {
  is_x: bool,
  revert: bool
}

fn set_visible_for_direction(grid: &mut Grid, dir: &Direction) -> () {
  
  let ( rng_1, rng_2 ): (Vec<usize>, Vec<usize>) = if dir.is_x {
    ((0..grid.height).collect(), if dir.revert { (0..grid.width).rev().collect() } else { (0..grid.width).collect() })
  } else {
    ((0..grid.width).collect(), if dir.revert { (0..grid.height).rev().collect() } else { (0..grid.height).collect() })
  };

  for &i in &rng_1 {
    let mut current_height: isize = -1;
    
    for &j in &rng_2 {
      let coord = if dir.is_x { Coord::new_usize(i,j) } else { Coord::new_usize(j,i) };
      if j == 0 || current_height < grid.values.get(&coord).unwrap().0 as isize {
        grid.values.get_mut(&coord).unwrap().1 = true;
      }
      let this_height = grid.values.get(&coord).unwrap().0 as isize;
      if this_height > current_height {
        current_height = this_height
      }
    };
  };
}



fn get_scenic_score_for_tree(grid: &Grid, initial_coord: &Coord) -> usize {
  let steps = vec![
    Coord(1,0),
    Coord(-1,0),
    Coord(0,1),
    Coord(0,-1),
  ];

  let mut trees_in_sight:Vec<usize> = vec![];
  let initial_height = grid.values.get(initial_coord).unwrap().0;

  for step in steps {
    let mut steps_to_take:isize = 1;
    
   
    while let Some((height, _v)) = grid.values.get(&(Coord(step.0, step.1) * steps_to_take + Coord(initial_coord.0, initial_coord.1))) {
      steps_to_take += 1;
      if *height >= initial_height { break; }
    };

    trees_in_sight.push(steps_to_take as usize - 1);
  }

  trees_in_sight.iter().fold(1, |acc, cur|{ acc * cur })
  
}


impl FromStr for Grid {
  type Err = String;


  fn from_str(s: &str) -> Result<Self, Self::Err> {
      let height = s.lines().count();
      let width = s.lines().next().unwrap().len();

      let mut hm:HashMap<Coord, (usize, bool)> = HashMap::new();
      
      s
      .lines()
      .enumerate()
      .for_each(|(y, line)|{
        line
        .chars()
        .enumerate()
        .for_each(|(x, c)|{
          hm.insert(Coord::new_usize(x,y.to_string().parse::<usize>().unwrap() ), (c.to_string().parse::<usize>().unwrap(), false));
        })
      });

      let mut result = Self { values: hm, height, width };

      vec![
        Direction { is_x: true, revert: false },
        Direction { is_x: true, revert: true },
        Direction { is_x: false, revert: false },
        Direction { is_x: false, revert: true },

      ].iter().for_each(|d|{
        set_visible_for_direction(&mut result, d);
      });

      Ok(result)
  }
}


pub const TEST_INPUT:&str = "30373
25512
65332
33549
35390";


#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn coords_can_be_added(){
    let a = Coord(1,2);
    let b = Coord(2,3);

    assert_eq!(a+b, Coord(3,5));

  }

  #[test]
  fn gets_correct_scenic_score(){
    let grid = TEST_INPUT.parse::<Grid>().unwrap();
    let result = get_scenic_score_for_tree(&grid, &Coord(2, 1));

    assert_eq!(result, 4);
  }
}
