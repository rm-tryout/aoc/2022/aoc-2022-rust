#![allow(dead_code)]

pub mod part_01;
pub mod part_02;
use std::{rc::Rc, collections::HashMap, cell::RefCell};

#[derive(Clone, Debug)]
pub struct Dir {
    parent: Option<Rc<RefCell<Dir>>>,
    subdirs: HashMap<String, Rc<RefCell<Dir>>>,
    size: usize
}

impl Dir {
    fn new() -> Dir {
        Dir {
            parent: None,
            subdirs: HashMap::new(),
            size: 0
        }
    }

    fn find_root(&self) -> Rc<RefCell<Dir>> {
        let mut current = Rc::new(RefCell::new(self.clone()));
        
        while let Some(next) = current.clone().borrow().parent.as_ref() {
          current = Rc::clone(&next);
        }

        Rc::clone(&current)
    }

    fn add_dir(&mut self, name: String, dir: Rc<RefCell<Dir>>) {
        self.subdirs.insert(name, dir);
    }

    fn get_size(&self) -> usize{
      self.size + self.subdirs
      .iter()
      .map(|(_name, dir)|{
        dir.borrow().get_size()
      })
      .sum::<usize>()
    }
}


pub fn get_tree(input: &str) -> Rc<RefCell<Dir>> {
  let root = Rc::new(RefCell::new(Dir::new()));
  let mut cwd = Rc::clone(&root);

  input
  .lines()
  .for_each(|l|{
    let words = l.split(" ").collect::<Vec<&str>>();
    match (words[0], words[1]){
      ("$", "ls") => { },
      ("$", "cd") => {
        match words[2] {
          ".." => cwd = Rc::clone(&cwd.clone().borrow().parent.as_ref().unwrap()),
          "/" => cwd = Rc::clone(&root),
          dirname => {
            cwd = Rc::clone(&cwd
              .clone()
              .borrow()
              .subdirs
              .get(dirname).unwrap()
            )
          },
        }
      }
      ("dir", dirname) => {
        let new_dir = Rc::new(RefCell::new(Dir {
          parent: Some(Rc::clone(&cwd)),
          size: 0,
          subdirs: HashMap::new()
        }));
        cwd.borrow_mut().add_dir(dirname.to_string(), new_dir.clone());
      },
      (filesize, _) => {
        cwd.borrow_mut().size += filesize.parse::<usize>().unwrap();
      }
    }
  });

  Rc::clone(&root)
}


pub const TEST_INPUT:&str = "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";