use std::{collections::HashMap, cell::RefCell, rc::Rc};

use crate::{get_tree, Dir};

fn add_candidates(current_search: &mut Vec<usize>, dirs: &HashMap<String, Rc<RefCell<Dir>>>, min_val: usize){
  for (_, folder) in dirs.iter(){
    if folder.borrow().get_size() >= min_val {
      current_search.push(folder.borrow().get_size())
    }

    add_candidates(current_search, &folder.borrow().subdirs, min_val)
  }
}

pub fn process_part_02(input:&str) -> usize{
  let mut deletion_candidates:Vec<usize> = vec![];
  let tree = get_tree(input);

  let diskspace_needed = 30000000 - (70000000 - tree.borrow().get_size());

  add_candidates(&mut deletion_candidates, &tree.borrow().subdirs, diskspace_needed);

  deletion_candidates.sort();
  deletion_candidates[0]
  
}
