#![allow(unused, dead_code)]

use std::{rc::Rc, cell::RefCell, borrow::BorrowMut, ops::Deref, collections::HashMap};

use crate::{Dir, get_tree};

fn scan_tree(current_search: &mut Vec<usize>, dirs: &HashMap<String, Rc<RefCell<Dir>>>, max_size: usize) {
  let folders = dirs
  .iter();

  for (name, dir) in folders {
    let size = dir.borrow().get_size();
    if size <= max_size {
      current_search.push(size);
    }

    scan_tree(current_search, &dir.borrow().subdirs, max_size);
  };
}

pub fn process_part_01(input: &str) -> usize {
  let tree = get_tree(input);
  let mut all_folders: Vec<usize> = vec![];

  scan_tree(&mut all_folders, &tree.borrow().subdirs, 100000);
  
  all_folders
  .iter()
  .fold(0 as usize, |acc, size|{
    acc + size
  })

  // println!("{:?}", result)
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn gets_example_right(){
    let result = process_part_01(TEST_INPUT);

    assert_eq!(result, 95437)
  }
}