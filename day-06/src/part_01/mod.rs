use crate::Stream;

pub fn process_part_01(input: &str) -> usize {
  let mut stream = Stream::new(input, 4);
  stream.find_marker()
}