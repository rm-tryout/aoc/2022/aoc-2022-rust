#![allow(dead_code, unused)]
use std::{str::{self, FromStr}, clone, collections::HashMap, ops::ControlFlow};

pub mod part_01;
pub mod part_02;

#[derive(PartialEq, Eq, Debug)]
struct Stream<'a> {
  value: &'a str,
  current: &'a str,
  position: usize,
  chunk_size: usize,
}

impl<'a> Stream<'a> {
  fn new(input: &'a str, chunk_size: usize) -> Self{
    Stream{
      value: input,
      chunk_size: chunk_size,
      current: &input[0..chunk_size],
      position: 0
    }
  }

  fn current_is_marker(&self) -> bool {
    let mut occurences:HashMap<char, usize> = HashMap::new();
    let mut is_marker = true;

    let cs = self.current
    .chars();
    
    for c in cs {
      if let Some(_) = occurences.get(&c) {
        is_marker = false;
        break;
      }
      occurences.insert(c, 1);
    }
    
   is_marker
  }

  fn find_marker(&mut self) -> usize {
    while !self.current_is_marker() {
      let next = self.next();
      if next == None {
        break;
      }
    } 

    self.position + self.chunk_size
  }
}

impl<'a> Iterator for Stream<'a> {
  type Item = &'a str;

  fn next(&mut self) -> Option<Self::Item> {
    self.position += 1;
    if self.position + self.chunk_size > self.value.len() {
      None
    } else {
      self.current = &self.value[self.position..self.position+self.chunk_size];
      
      Some(self.current)
    }
  }
}

pub const TEST_INPUT:&str = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn stream_will_correctly_return_first_chunk(){
    let mut stream = Stream::new("1234567", 4);
    let result = stream.current;

    assert_eq!(result, "1234".to_string())
  }

  #[test]
  fn returns_none_if_string_is_too_short(){
    let mut stream = Stream::new("1234", 4);
    let result = stream.next();

    assert_eq!(result, None);
  }

  #[test]
  fn returns_3456_for_second_iteration(){
    let mut stream = Stream::new("1234567", 4);
    let result = stream.nth(1).unwrap();

    assert_eq!(result, "3456".to_string())
  }

  #[test]
  fn correctly_finds_marker(){
    let mut stream = Stream::new(TEST_INPUT, 4);
    let result = stream.find_marker();

    assert_eq!(result, 7);
  }
}