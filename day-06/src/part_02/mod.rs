use crate::Stream;

pub fn process_part_02(input:&str) -> usize {
  let mut stream = Stream::new(input, 14);
  stream.find_marker()
}