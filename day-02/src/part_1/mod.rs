use crate::{Hand, play_round};

pub fn process_part_01(input:&str) -> u32 {
  input
  .lines()
  .map(|line| {
    let hands = line.split(" ").collect::<Vec<&str>>();
    play_round(
      &hands[0].parse::<Hand>().unwrap(), 
      &hands[1].parse::<Hand>().unwrap()
    )
  })
  .sum::<u32>()
}

#[cfg(test)]
mod tests {
    use crate::TEST_INPUT;
    use super::*;

    #[test]
    fn processing_part_01_works() {
      let result = process_part_01(TEST_INPUT);
      assert_eq!(result, 15);
    }
}
