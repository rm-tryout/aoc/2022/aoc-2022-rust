use std::{fs, time::Instant};

use day_02::{part_1, part_2};

fn main(){
  let input = fs::read_to_string("./input.txt").unwrap();
  let start_01 = Instant::now();
  let result_01 = part_1::process_part_01(&input);
  println!("
  Result of part 1:   {:?}
  Execution time was: {:?}
  ", result_01, Instant::now() - start_01);

  let start_02 = Instant::now();
  let result_02 = part_2::process_part_02(&input);
  println!("
  Result of part 2:   {:?}
  Execution time was: {:?}
  ", result_02, Instant::now() - start_02);
}