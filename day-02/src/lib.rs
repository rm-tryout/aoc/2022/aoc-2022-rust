use std::{str::FromStr, cmp::Ordering};

pub mod part_1;
pub mod part_2;

#[derive(PartialEq, Clone, Copy)]
pub enum Hand {
  Rock = 1,
  Paper = 2,
  Scissors = 3,
}

impl FromStr for Hand {
  type Err = String;
  fn from_str(s: &str) -> Result<Self, Self::Err> {
      match s {
        "A" | "X" => Ok(Hand::Rock),
        "B" | "Y" => Ok(Hand::Paper),
        "C" | "Z" => Ok(Hand::Scissors),
        _ => Err("unknown hand".to_string())
      }
  }
}

impl ToString for Hand {
  fn to_string(&self) -> String {
      "test".to_string()
  }
}

impl PartialOrd for Hand {
  fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
      let result = match (self, other){
        (Hand::Paper, Hand::Rock) |
        (Hand::Scissors, Hand::Paper) |
        (Hand::Rock, Hand::Scissors) => Ordering::Greater,
        (Hand::Rock, Hand::Paper) |
        (Hand::Scissors, Hand::Rock) |
        (Hand::Paper, Hand::Scissors) => Ordering::Less,
        (a, b) if a==b => Ordering::Equal,
        _ => panic!("unknown hand combination")
      };

      Some(result)
  }
}

pub fn play_round(opponents_hand:&Hand, your_hand:&Hand) -> u32 {

  match your_hand.partial_cmp(&opponents_hand).unwrap() {
    Ordering::Less => *your_hand as u32,
    Ordering::Equal => 3 + *your_hand as u32,
    Ordering::Greater => 6 + *your_hand as u32,
  }
}

pub const TEST_INPUT:&str = "A Y
B X
C Z
";