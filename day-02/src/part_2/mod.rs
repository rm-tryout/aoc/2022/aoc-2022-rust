use crate::play_round;
use super::Hand;

impl Hand {
  fn get_hand_by_instruction(opponents_hand: &Hand, instruction:&str) -> Hand {
    match instruction {
      "X" => match opponents_hand {
        Hand::Paper => Hand::Rock,
        Hand::Rock => Hand::Scissors,
        Hand::Scissors => Hand::Paper
      },
      "Y" => opponents_hand.clone(),
      "Z" => match opponents_hand {
        Hand::Paper => Hand::Scissors,
        Hand::Scissors => Hand::Rock,
        Hand::Rock => Hand::Paper
      },
      _ => panic!("unknown instruction")

    }
  }
}

pub fn process_part_02(input: &str) -> u32 {
  input
  .lines()
  .map(|line| {
    let lines: Vec<&str> = line.split(" ").collect();
    let opponents_hand = lines[0].parse::<Hand>().unwrap();
    
    play_round(
      &opponents_hand, 
      &Hand::get_hand_by_instruction(&opponents_hand, lines[1])
    )
    
  })
  .sum()
}

#[cfg(test)]
mod tests {
  use crate::TEST_INPUT;
  use super::*;

  #[test]
  fn processing_part_02_works(){
    let result = process_part_02(TEST_INPUT);
    assert_eq!(result, 12)
  }
}