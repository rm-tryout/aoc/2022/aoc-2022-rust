pub fn process_part1(input: &str) -> String {
  let result = input
  .split("\n\n")
  .map(|elf_load| {
    elf_load
    .lines()
    .map(|item| item.parse::<u32>().unwrap())
    .sum::<u32>()

  })
  .max()
  .unwrap();

  result.to_string()
}


pub fn process_part2 (input: &str) -> String {

  let mut elf_loads = input
  .split("\n\n")
  .map(|elf_load| {
    elf_load
    .lines()
    .map(|line| {
      line.parse::<u32>().unwrap()
    })
    .sum::<u32>()
  })
  .collect::<Vec<u32>>();
  
  elf_loads.sort_by(|a,b| b.cmp(a));
  
  elf_loads
  .iter()
  .take(3)
  .sum::<u32>()
  .to_string()
  
}

mod tests {
  use super::*;

  #[test]
  fn part_01_works(){
    let input="1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";
    
    let result = process_part1(input);

    assert_eq!(result, "24000");
  }
  #[test]
  fn part_02_works(){
    let input="1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";

    let result = process_part2(input);

    assert_eq!(result, "45000");
  }
}