use crate::get_prio_from_line;

pub fn process_part_01(input:&str) -> usize {
  input
  .lines()
  .map(|line| {
    get_prio_from_line(line)
  })
  .sum()
}