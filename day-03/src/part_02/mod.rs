use crate::get_priority;

pub fn process_part_02(input:&str) -> usize {
  input
  .lines()
  .collect::<Vec<&str>>()
  .chunks(3)
  .map(|group| {
    for char in group[0].chars() {
      if group[1].contains(char) && group[2].contains(char) {
        return get_priority(&char)
      }
    }

    panic!("could not find matching character")
  })
  .sum()
}