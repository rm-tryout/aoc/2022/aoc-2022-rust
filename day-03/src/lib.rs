pub mod part_01;
pub mod part_02;

pub fn get_priority(input: &char) -> usize {
    let mut characters = vec![];
    characters.extend(('a'..='z').collect::<Vec<char>>());
    characters.extend(('A'..='Z').collect::<Vec<char>>());
    
    if let Some(index) = characters.iter().position(|l| l == input) {
        return index + 1;
    }

    panic!("input {} is neither uppercase nor lowercase character", input);
}

pub fn get_prio_from_line(line:&str) -> usize {
    let comp1 = &line[..line.len()/2];
    let comp2 = &line[line.len()/2..];

    for char in comp1.chars(){
        if comp2.contains(char) {
            return get_priority(&char);
        }
    }

    panic!("could not find the priority in the line");
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn get_priority_works_with_lowercase(){
        let result = get_priority(&'c');
        assert_eq!(result, 3);
    }

    #[test]

    fn get_priority_works_with_uppercase(){
        let result = get_priority(&'P');
        assert_eq!(result, 42);
    }
}