

use std::ops::RangeInclusive;

use crate::line_to_ranges;

pub fn range_contains_other(r1: &RangeInclusive<usize>, r2: &RangeInclusive<usize>) -> bool{
  r1.start() <= r2.start() && r1.end() >= r2.end()
  ||
  r2.start() <= r1.start() && r2.end() >= r1.end()
}

pub fn process_part_01(input: &str) -> usize{
  let mut limit = 0 as usize;

  input
  .lines()
  .filter(|&line|{
    let ranges = line_to_ranges(line);
    if ranges.2 > limit{
      limit = ranges.2
    }

    range_contains_other(&ranges.0, &ranges.1)
  })
  .count()
}

#[cfg(test)]

mod test {
  use super::*;

  #[test]
  fn range_contains_other_correctly_returns_true(){
    let result = range_contains_other(&(2..=4), &(3..=4));
    assert_eq!(result, true);
  }

  #[test]
  fn range_contains_other_correctly_returns_false(){
    let result = range_contains_other(&(2..=4), &(3..=5));
    assert_eq!(result, false);
  }
}