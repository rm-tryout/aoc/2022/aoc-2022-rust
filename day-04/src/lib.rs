use std::ops::RangeInclusive;

pub mod part_01;
pub mod part_02;

pub const TEST_INPUT:&str = "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
";

#[derive(PartialEq, Eq, Debug)]
pub struct RangesAndLimit(RangeInclusive<usize>, RangeInclusive<usize>, usize);

pub fn line_to_ranges(line:&str) -> RangesAndLimit {
  let vec = line.split(',')
  .map(|r| {
    let vec = r.split("-")
      .map(|s| 
        s.parse::<usize>().unwrap()
      ).collect::<Vec<usize>>();
      
    vec[0]..=vec[1]
  })
  .collect::<Vec<RangeInclusive<usize>>>();

  let limit = if vec[0].end() > vec[1].end() {
    vec[0].end()
  } else {
    vec[1].end()
  };
  
  RangesAndLimit(vec[0].clone(), vec[1].clone(), *limit)
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn line_to_ranges_works(){
    let result = line_to_ranges("2-4,4-5");
    assert_eq!(result, RangesAndLimit(2..=4, 4..=5, 5));
  }
}