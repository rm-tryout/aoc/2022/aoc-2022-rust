use std::ops::RangeInclusive;
use crate::line_to_ranges;

pub fn range_overlaps(r1: &RangeInclusive<usize>, r2: &RangeInclusive<usize>) -> bool{
  r1.start() >= r2.start() && r1.start() <= r2.end()
  ||
  r2.start() >= r1.start() && r2.start() <= r1.end()
}

pub fn process_part_02(input:&str) -> usize {
  let mut limit = 0 as usize;

  input
  .lines()
  .filter(|&line|{
    let ranges = line_to_ranges(line);
    if ranges.2 > limit{
      limit = ranges.2
    }

    range_overlaps(&ranges.0, &ranges.1)
  })
  .count()
}


#[cfg(test)]

mod test {
  use super::*;


  #[test]
  fn range_overlaps_correcly_returns_true(){
    let result = range_overlaps(&(0..=2), &(2..=4));
    assert_eq!(result, true);
  }

  #[test]
  fn range_overlaps_correctly_returns_false(){
    let result = range_overlaps(&(0..=2), &(3..=4));
    assert_eq!(result, false);
  }
}