use crate::{parse_stack, parse_instructions, Instruction};

pub fn execute_move<'a>(stack: &'a mut Vec<Vec<&str>>, mov: &'a Instruction) -> &'a Vec<Vec<&'a str>> {
  for _ in 0..mov.nr_of_crates {
    let in_transit = stack[mov.from].pop().unwrap();
    stack[mov.to].push(in_transit)
  };

  stack
}

pub fn process_part_01(input: &str) -> String {
  let mut stacks = parse_stack(&input);
  
  parse_instructions(&input)
  .iter()
  .for_each(|inst|{
    execute_move(&mut stacks, inst);
  });

  let mut s = String::new();
  for stack in stacks {
    s.push(stack.last().unwrap().chars().nth(1).unwrap());
  }
  s
}

#[cfg(test)]
mod test {
  use crate::TEST_INPUT;

use super::*;

  #[test]
  fn execute_instruction_works_as_expected(){
    let mut stack = parse_stack(&TEST_INPUT);
    let instruction = "move 1 from 2 to 1".parse::<Instruction>().unwrap();
    let result = execute_move(&mut stack, &instruction);

    assert_eq!(result[0], vec!["[Z]","[N]","[D]"]);
  }
}