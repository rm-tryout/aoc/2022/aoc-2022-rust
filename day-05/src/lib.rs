pub mod part_01;
pub mod part_02;

use std::str::{self, FromStr};

pub struct Instruction{
  nr_of_crates: usize,
  from: usize,
  to: usize
}

impl FromStr for Instruction {
    type Err = String;
    
    fn from_str(s: &str) -> Result<Self, Self::Err> {
      let split = s
      .split(" ")
      .collect::<Vec<&str>>();

      Ok(Instruction {
        nr_of_crates: split[1].parse::<usize>().unwrap(), 
        from: split[3].parse::<usize>().unwrap() - 1, 
        to: split[5].parse::<usize>().unwrap() - 1 
      })
    }
}

pub fn parse_stack(input:&str) -> Vec<Vec<&str>>{
  let stack_count = (input.lines().take(1).last().unwrap().len() + 1) / 4;

  let mut stacks:Vec<Vec<&str>> = vec![
    Vec::new();
    stack_count
  ];

  input
  .split(" 1")
  .next()
  .unwrap()  
  .lines()
  .for_each(|l|
    l
    .as_bytes()
    .chunks(4)
    .map(str::from_utf8)
    .enumerate()
    .for_each(|(a,b)|{
      if !b.unwrap().contains("   ") {
        stacks[a%stack_count].insert(0,b.unwrap().trim())
      }
    })
  );

  stacks
}

pub fn parse_instructions(input:&str) -> Vec<Instruction> {
  input
  .split("\n\n")
  .last()
  .unwrap()
  .lines()
  .map(|l|{
    l.parse::<Instruction>().unwrap()
  })
  .collect()
}

pub const TEST_INPUT:&str = "    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";

